import type { PiletApi } from "shell";
import { fromVue3 } from "piral-vue-3/convert";

import Page from "./Page.vue";

export function setup(app: PiletApi) {
  app.registerPage("/", fromVue3(Page));
}
