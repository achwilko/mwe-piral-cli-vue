const pluginVue = require("@vitejs/plugin-vue");

module.exports = function (config) {
  config.plugins.push(pluginVue());
  return config;
};
