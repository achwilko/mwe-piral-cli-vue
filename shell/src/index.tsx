import * as React from "react";
import { createRoot } from "react-dom/client";
import { createInstance, Piral } from "piral";

const instance = createInstance({
  state: {
    components: {
      Layout: ({ children }) => <div className="shell">{children}</div>,
      ErrorInfo: ({ type }) => <>Error: {type}</>,
    },
  },
  plugins: [],
});

const root = createRoot(document.querySelector("#app"));
root.render(<Piral instance={instance} />);
